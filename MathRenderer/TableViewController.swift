//
//  TableViewController.swift
//  MathRenderer
//
//  Created by Haldun Bayhantopcu on 24/02/15.
//  Copyright (c) 2015 Simpli. All rights reserved.
//

import UIKit

class TableViewController: UITableViewController, UIWebViewDelegate {
  let rows = [
    "<style>* { font-size: 32px }</style><math xmlns='http://www.w3.org/1998/Math/MathML' display='block'><mi>x</mi><mo>=</mo><mfrac><mrow><mo>-</mo><mi>b</mi><mo>&plusmn;</mo><msqrt><msup><mi>b</mi><mn>2</mn></msup><mo>&minus;</mo><mrow><mn>4</mn><mi>a</mi><mi>c</mi></mrow></msqrt></mrow><mrow><mn>2</mn><mi>a</mi></mrow></mfrac></math>",
    "<style>* { font-size: 14px }</style><center><math xmlns='http://www.w3.org/1998/Math/MathML'><mrow><mfrac><mrow><msup><mrow><mi>x</mi></mrow><mrow><mn>2</mn></mrow></msup><mo>+</mo><mn>2</mn><mo>&it;</mo><msqrt><mrow><mn>5.12</mn><mo>·</mo><msup><mrow><mn>10</mn></mrow><mrow><mn>3</mn></mrow></msup><mo>+</mo><mi>c</mi></mrow></msqrt></mrow><mrow><mn>12</mn><mo>&it;</mo><mi>j</mi><mo>+</mo><mrow><msubsup><mi>n</mi><mrow><mn>1</mn></mrow><mrow><mn>2</mn></mrow></msubsup></mrow><mo>+</mo><mfrac><mrow><mi>y</mi></mrow><mrow><mn>4</mn></mrow></mfrac><mo>-</mo><mn>1</mn></mrow></mfrac><mo>+</mo><mover><mi>h</mi><mo>&HorizontalLine;</mo></mover><mo>&RightArrow;</mo><mn>0</mn></mrow></math></center>",
    "<math title=\"2^(2/(sqrt(6))\" xmlns=\"http://www.w3.org/1998/Math/MathML\"><mstyle mathcolor=\"black\" fontfamily=\"sans-serif\" displaystyle=\"true\"><msup><mn>2</mn><mrow><mfrac><mn>2</mn><mrow><msqrt><mrow><mn>6</mn></mrow></msqrt></mrow></mfrac></mrow></msup></mstyle></math>",
    "<math xmlns='http://www.w3.org/1998/Math/MathML'><mrow><mfenced open='[' close=']'><mrow><mtable><mtr><mtd><mi>a</mi></mtd><mtd><mi>b</mi></mtd></mtr><mtr><mtd><mi>c</mi></mtd><mtd><mi>d</mi></mtd></mtr></mtable></mrow></mfenced></mrow></math>",
  ]
  
  var readyRows = [String]()
  var rowHeights = [Int:CGFloat]()

  override func viewDidLoad() {
    super.viewDidLoad()

    for (index, row) in enumerate(rows) {
      let webView = UIWebView(frame: view.bounds)
      webView.delegate = self
      webView.hidden = true
      webView.loadHTMLString(row, baseURL: nil)
      webView.tag = index
      view.addSubview(webView)
    }
  }
  
  func webViewDidFinishLoad(webView: UIWebView) {
    webView.scrollView.scrollEnabled = false
    var frame = webView.frame
    frame.size.height = 1
    webView.frame = frame
    let height = webView.scrollView.contentSize.height
    rowHeights[webView.tag] = height
    readyRows.append(rows[webView.tag])    
    if readyRows.count == rows.count {
      tableView.reloadData()
    }
    webView.removeFromSuperview()
  }
  
  // MARK: - Table view data source

  override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return readyRows.count
  }
  
  override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
    return rowHeights[indexPath.row] ?? 60.0
  }

  override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as TableViewCell
    let htmlString = readyRows[indexPath.row]
    cell.webView.scrollView.scrollEnabled = false
    cell.webView.loadHTMLString(htmlString, baseURL: nil)
    return cell
  }
}
