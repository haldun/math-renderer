//
//  TableViewCell.swift
//  MathRenderer
//
//  Created by Haldun Bayhantopcu on 24/02/15.
//  Copyright (c) 2015 Simpli. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {
  
  @IBOutlet weak var webView: UIWebView!
  

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
